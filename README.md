## Live Camera Proof of concept ##

This is a POC to demonstrate having a live camera feed inside a fragment/activity of our app and directly capture and store these photos to device.

We could use this on the main page for our app , where the user can click a selfie and then we show them further options to post it.